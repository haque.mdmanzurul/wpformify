<?php

/**
 * @wordpress-plugin
 * Plugin Name:       WP Formify
 * Plugin URI:        http://wpformify.com/
 * Description:       A next generation drag and drop form plugin.
 * Version:           1.0.0
 * Author:            AJTECH
 * Author URI:        http://ajtechbd.com/
 * License:           MIT
 */

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/vendor/getherbert/framework/bootstrap/autoload.php';
