<?php namespace WpFormify;

/** @var \Herbert\Framework\Panel $panel */

// All Forms
$panel->add([
    'type'   => 'panel',
    'as'     => 'mainPanel',
    'title'  => 'Formify',
    'rename' => 'All Froms',
    'slug'   => 'wp-formify',
    'uses'   => function()
    {
        return 'Hello World';
    }
]);


// New Form or Form Builder UI
$panel->add([
    'type'   => 'sub-panel',
    'parent' => 'mainPanel',
    'as'     => 'formifybuilder',
    'title'  => 'New Form',
    'slug'   => 'formify-newform',
    'uses'   => function()
    {
        return 'Form Builder UI';
    }
]);


$panel->add([
    'type'   => 'sub-panel',
    'parent' => 'mainPanel',
    'as'     => 'formifysubmissions',
    'title'  => 'Submissions',
    'slug'   => 'formify-submissions',
    'uses'   => function()
    {
        return 'Form Submission UI';
    }
]);

$panel->add([
    'type'   => 'sub-panel',
    'parent' => 'mainPanel',
    'as'     => 'formifyimportexport',
    'title'  => 'Import & Export',
    'slug'   => 'formify-importexport',
    'uses'   => function()
    {
        return 'Form Import Export UI';
    }
]);

$panel->add([
    'type'   => 'sub-panel',
    'parent' => 'mainPanel',
    'as'     => 'formifysettings',
    'title'  => 'Settings',
    'slug'   => 'formify-setting',
    'uses'   => function()
    {
        return 'Setting UI';
    }
]);

$panel->add([
    'type'   => 'sub-panel',
    'parent' => 'mainPanel',
    'as'     => 'formifyreports',
    'title'  => 'Reports & Analytics',
    'slug'   => 'formify-report',
    'uses'   => function()
    {
        return 'Reports UI';
    }
]);


$panel->add([
    'type'   => 'sub-panel',
    'parent' => 'mainPanel',
    'as'     => 'formifyaddons',
    'title'  => 'Addons',
    'slug'   => 'formify-addons',
    'uses'   => function()
    {
        return 'Addon Installation UI';
    }
]);